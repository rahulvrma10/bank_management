-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.43-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema bankmanagement
--

CREATE DATABASE IF NOT EXISTS bankmanagement;
USE bankmanagement;

--
-- Definition of table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `user` varchar(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  `access` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` (`user`,`password`,`access`) VALUES 
 ('admin','1234','admin'),
 ('manage','1234','manager'),
 ('manager','1234','manager'),
 ('qw','1212','user'),
 ('rahul','1234','admin'),
 ('ravi','1234','admin'),
 ('thakur','1234','user');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;


--
-- Definition of table `new_account`
--

DROP TABLE IF EXISTS `new_account`;
CREATE TABLE `new_account` (
  `account` varchar(10) NOT NULL,
  `name` varchar(10) DEFAULT NULL,
  `sex` varchar(7) DEFAULT NULL,
  `age` varchar(3) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `open_date` date DEFAULT NULL,
  `deposit_date` date DEFAULT NULL,
  `withdraw_date` date DEFAULT NULL,
  `last_deposit` varchar(20) DEFAULT NULL,
  `last_withdraw` varchar(20) DEFAULT NULL,
  `balance` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `new_account`
--

/*!40000 ALTER TABLE `new_account` DISABLE KEYS */;
INSERT INTO `new_account` (`account`,`name`,`sex`,`age`,`mobile`,`address`,`open_date`,`deposit_date`,`withdraw_date`,`last_deposit`,`last_withdraw`,`balance`) VALUES 
 ('10000','rahul','male','12','1231231231','213wqe','2014-07-31','2014-07-31','2014-07-31','123','12','111');
/*!40000 ALTER TABLE `new_account` ENABLE KEYS */;


--
-- Definition of table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `account` varchar(50) DEFAULT NULL,
  `action` varchar(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount` varchar(20) DEFAULT NULL,
  KEY `account` (`account`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`account`) REFERENCES `new_account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`account`,`action`,`date`,`amount`) VALUES 
 ('10000','Deposit','2014-07-31','123'),
 ('10000','Withdrawl','2014-07-31','12');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
